Version of this patch from upstream

--- keepalived-2.2.8/keepalived/core/commit e78513fe0ce5d83c226ea2c0bd222f375c2438e7
Author: Quentin Armitage <quentin@armitage.org.uk>
Date:   Fri Jul 12 15:16:47 2024 +0100

    vrrp: Handle empty ipset names with vrrp_ipsets keyword
    
    We now handle empty ipset names and return a config error.
    
    Signed-off-by: Quentin Armitage <quentin@armitage.org.uk>


global_parser.c	2023-04-01 18:39:25.000000000 +0100
+++ keepalived-2.2.8.patched/keepalived/core/global_parser.c	2024-11-28 08:56:17.445615602 +0000
@@ -1086,6 +1086,22 @@
 	}
 }
 #ifdef _HAVE_LIBIPSET_
+static bool
+check_valid_ipset_name(const vector_t *strvec, unsigned entry, const char *log_name)
+{
+	if (strlen(strvec_slot(strvec, entry)) >= IPSET_MAXNAMELEN - 1) {
+		report_config_error(CONFIG_GENERAL_ERROR, "VRRP Error : ipset %s name too long - ignored", log_name);
+		return false;
+	}
+
+	if (strlen(strvec_slot(strvec, entry)) == 0) {
+		report_config_error(CONFIG_GENERAL_ERROR, "VRRP Error : ipset %s name empty - ignored", log_name);
+		return false;
+	}
+
+	return true;
+}
+
 static void
 vrrp_ipsets_handler(const vector_t *strvec)
 {
@@ -1103,17 +1119,13 @@
 		return;
 	}
 
-	if (strlen(strvec_slot(strvec,1)) >= IPSET_MAXNAMELEN - 1) {
-		report_config_error(CONFIG_GENERAL_ERROR, "VRRP Error : ipset address name too long - ignored");
+	if (!check_valid_ipset_name(strvec, 1, "address"))
 		return;
-	}
 	global_data->vrrp_ipset_address = STRDUP(strvec_slot(strvec,1));
 
 	if (vector_size(strvec) >= 3) {
-		if (strlen(strvec_slot(strvec,2)) >= IPSET_MAXNAMELEN - 1) {
-			report_config_error(CONFIG_GENERAL_ERROR, "VRRP Error : ipset IPv6 address name too long - ignored");
+		if (!check_valid_ipset_name(strvec, 2, "IPv6 address"))
 			return;
-		}
 		global_data->vrrp_ipset_address6 = STRDUP(strvec_slot(strvec,2));
 	}
 	else {
@@ -1124,10 +1136,8 @@
 		global_data->vrrp_ipset_address6 = STRDUP(set_name);
 	}
 	if (vector_size(strvec) >= 4) {
-		if (strlen(strvec_slot(strvec,3)) >= IPSET_MAXNAMELEN - 1) {
-			report_config_error(CONFIG_GENERAL_ERROR, "VRRP Error : ipset IPv6 address_iface name too long - ignored");
+		if (!check_valid_ipset_name(strvec, 3, "IPv6 address_iface"))
 			return;
-		}
 		global_data->vrrp_ipset_address_iface6 = STRDUP(strvec_slot(strvec,3));
 	}
 	else {
@@ -1142,10 +1152,8 @@
 	}
 
 	if (vector_size(strvec) >= 5) {
-		if (strlen(strvec_slot(strvec,4)) >= IPSET_MAXNAMELEN - 1) {
-			report_config_error(CONFIG_GENERAL_ERROR, "VRRP Error : ipset IGMP name too long - ignored");
+		if (!check_valid_ipset_name(strvec, 4, "IGMP"))
 			return;
-		}
 		global_data->vrrp_ipset_igmp = STRDUP(strvec_slot(strvec,4));
 	}
 	else {
@@ -1156,10 +1164,8 @@
 		global_data->vrrp_ipset_igmp = STRDUP(set_name);
 	}
 	if (vector_size(strvec) >= 6) {
-		if (strlen(strvec_slot(strvec,5)) >= IPSET_MAXNAMELEN - 1) {
-			report_config_error(CONFIG_GENERAL_ERROR, "VRRP Error : ipset MLD name too long - ignored");
+		if (!check_valid_ipset_name(strvec, 5, "MLD"))
 			return;
-		}
 		global_data->vrrp_ipset_mld = STRDUP(strvec_slot(strvec,5));
 	}
 	else {
